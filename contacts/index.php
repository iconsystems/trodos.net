<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?><div class="contacts_map">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view", 
	"map", 
	array(
		"API_KEY" => "",
		"COMPONENT_TEMPLATE" => "map",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CONTROLS" => array(
			0 => "ZOOM",
			1 => "SMALLZOOM",
		),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:54.829394928964874;s:10:\"yandex_lon\";d:75.69493739855305;s:12:\"yandex_scale\";i:4;s:10:\"PLACEMARKS\";a:2:{i:0;a:3:{s:3:\"LON\";d:82.95870855187;s:3:\"LAT\";d:55.000936082606;s:4:\"TEXT\";s:82:\"г. Новосибирск, ул. Большевистская, 131 корпус 6\";}i:1;a:3:{s:3:\"LON\";d:37.731586215456;s:3:\"LAT\";d:55.477564467768;s:4:\"TEXT\";s:120:\"Московская обл, г. Домодедово, ул. Логистическая, 1/8 подъезд 4, оф. 325\";}}}",
		"MAP_HEIGHT" => "400",
		"MAP_ID" => "",
		"MAP_WIDTH" => "100%",
		"OPTIONS" => array(
			0 => "ENABLE_DBLCLICK_ZOOM",
			1 => "ENABLE_DRAGGING",
		),
		"ZOOM_BLOCK" => array(
			"POSITION" => "right center",
		)
	),
	false
);?>
</div>
<div class="wrapper_inner">
	<div class="contacts_left">
		<div class="store_description">
			<div class="store_property">
				<div class="title">
					Офис/склад:
				</div>
				<div class="value">
					 <?$APPLICATION->IncludeFile(SITE_DIR."include/address.php", Array(), Array("MODE" => "html", "NAME" => "Адрес"));?>
				</div>
			</div>
			<div class="store_property">
				<div class="title">
					Телефон
				</div>
				<div class="value">
					 <?$APPLICATION->IncludeFile(SITE_DIR."include/phone.php", Array(), Array("MODE" => "html", "NAME" => "Телефон"));?>
				</div>
			</div>
			<div class="store_property">
				<div class="title">
					Email
				</div>
				<div class="value">
					 <?$APPLICATION->IncludeFile(SITE_DIR."include/email.php", Array(), Array("MODE" => "html", "NAME" => "Email"));?>
				</div>
			</div>
			<div class="store_property">
				<div class="title">
					Режим работы
				</div>
				<div class="value">
					 <?$APPLICATION->IncludeFile(SITE_DIR."include/schedule.php", Array(), Array("MODE" => "html", "NAME" => "Время работы"));?>
				</div>
			</div>
		</div>
	</div>
	<div class="contacts_right">
		<blockquote>
			<?$APPLICATION->IncludeFile(SITE_DIR."include/contacts_text.php", Array(), Array("MODE" => "html", "NAME" => GetMessage("CONTACTS_TEXT")));?>
		</blockquote>
		 <?Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("form-feedback-block");?> <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"inline",
	Array(
		"CACHE_TIME" => "3600000",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "?send=ok",
		"USE_EXTENDED_ERRORS" => "Y",
		"VARIABLE_ALIASES" => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
		"WEB_FORM_ID" => "3"
	)
);?> <?Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("form-feedback-block", "");?>
	</div>
</div>
<div class="clearboth">
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>