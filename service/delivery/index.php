<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Условия доставки");
?><p style="text-align: justify;">
	Для вашего удобства предлагаем несколько вариантов возможной доставки:
</p>
<p style="text-align: justify;">
</p>
<ul style="text-align: justify;">
	<li>самовывоз с нашего склада в Новосибирске или Москве.</li>
	<li>доставка транспортной компанией.</li>
	<li>контейнерная перевозка.</li>
	<li>бесплатная доставка товара по Новосибирску (при сумме заказа от 10&nbsp;000 руб).</li>
	<li>бесплатная доставка товара по Москве (подробности уточняйте у наших специалистов)</li>
</ul>
<p>
</p>
<h2 style="text-align: justify;">
Срок доставки </h2>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 В зависимости от вашего региона проживания, доставка занимает разное время. О доставке в другие регионы России и страны СНГ уточняйте информацию у наших специалистов.
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>