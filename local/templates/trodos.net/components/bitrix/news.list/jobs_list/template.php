<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>

<div class="wrap_md job-banner-block">
	<img src="<?php echo SITE_TEMPLATE_PATH ?>/images/job-banner.jpg" alt="" title="">
	<div class="iblock but job-button">
		<a class="button vbig_btn wides resume_send" data-jobs="<?=$arItem['NAME']?>">
				<span><?=GetMessage('SEND_RESUME')?></span>
		</a>
	</div>
	</div>


<?if($arResult["ITEMS"]){?>
	<h3 class="jobs"><?=GetMessage("ACTUAL_VACANCY");?></h3>
	<div class="jobs_wrapp">
		<?foreach($arResult["ITEMS"] as $key => $arItem){
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="name">
					<table>
						<tr>
							<td class="title">
								<h4><span class="link"><?=$arItem['NAME']?></span></h4>
								<div class="salary">
									<?if ($arItem["DISPLAY_PROPERTIES"]["SALARY"]){?>
										<?=GetMessage("SALARY");?>
										<?if(is_numeric($arItem["DISPLAY_PROPERTIES"]["SALARY"]["VALUE"])) {
										    echo number_format($arItem["DISPLAY_PROPERTIES"]["SALARY"]["VALUE"], 0, "", " ");
										}
										else {
										    echo $arItem["DISPLAY_PROPERTIES"]["SALARY"]["VALUE"];
										}?>
										<?=($arItem["DISPLAY_PROPERTIES"]["CURRENCY"]["VALUE"] ? $arItem["DISPLAY_PROPERTIES"]["CURRENCY"]["VALUE"] : GetMessage("CURRENCY") );?>
									<?}else{?>
										<?=GetMessage("NOT_SALARY");?>
									<?}?>
								</div>
							</td>
							<td class="salary_wrapp">
								<div class="salary">
									<?if ($arItem["DISPLAY_PROPERTIES"]["SALARY"]){?>
										<?=GetMessage("SALARY");?>
										<?if(is_numeric($arItem["DISPLAY_PROPERTIES"]["SALARY"]["VALUE"])) {
										    echo number_format($arItem["DISPLAY_PROPERTIES"]["SALARY"]["VALUE"], 0, "", " ");
										}
										else {
										    echo $arItem["DISPLAY_PROPERTIES"]["SALARY"]["VALUE"];
										}?>
										<?=($arItem["DISPLAY_PROPERTIES"]["CURRENCY"]["VALUE"] ? $arItem["DISPLAY_PROPERTIES"]["CURRENCY"]["VALUE"] : GetMessage("CURRENCY") );?>
									<?}else{?>
										<?=GetMessage("NOT_SALARY");?>
									<?}?>
								</div>
							</td>
							<td class="icon">
								<span class="slide opener_icon no_bg"><i></i></span>
							</td>
						</tr>
					</table>
				</div>
				<div class="description_wrapp" >
					<?if ($arItem['PREVIEW_TEXT']):?>
						<div class="description"><?=$arItem['PREVIEW_TEXT']?></div>
					<?elseif ($arItem['DETAIL_TEXT']): ?>
						<div class="description"><?=$arItem['DETAIL_TEXT']?></div>
					<?endif;?>
					<a class="button vbig_btn wides resume_send" data-jobs="<?=$arItem['NAME']?>">
						<span><?=GetMessage('SEND_RESUME')?></span>
					</a>
				</div>
			</div>
		<?}?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]){?>
			<?=$arResult["NAV_STRING"]?>
		<?}?>
	</div>
<?}?>

<style>
/*+Айтиллект [Ковалева Ю.С.] Баннер в вакансиях*/
.job-banner-block {position:relative; margin-bottom:20px;}
.job-banner-block img {width:100%;}
.job-button {position:absolute; bottom:10px; left:0px;}
@media(max-width: 600px) {
.job-banner-block {text-align:center; padding:20px; border: 1px solid #e5e5e5; }
.job-banner-block img {display:none;}
.job-button {position:relative; bottom:0px; left:0px;right:0px;top:0px; width:100%}
/*-Айтиллект [Ковалева Ю.С.] Баннер в вакансиях*/
</style>