<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
	'NAME' => 'Импорт пользователей 1С',
	'DESCRIPTION' => 'Компонент импорта пользователей из 1С в Битрикс',
	'PATH' => array('ID' => 'Айтиллект')
);
?>