<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
function generate_password($input = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,.<>/?;:.\'"[]{}\|`~!@#$%^&*()-_+=', $strlength = 10)
{
	$input_length = strlen($input);
	$random_string = '';
	for($i = 0; $i < $strlength; $i++)
	{
		$random_character = $input[mt_rand(0, $input_length - 1)];
		$random_string .= $random_character;
	}
	return $random_string;
}
function UserPropsValue($profileID, $orderPropsID, $name, $value)
{
	CSaleOrderUserPropsValue::Add(array(
	   'USER_PROPS_ID' => $profileID,
	   'ORDER_PROPS_ID' => $orderPropsID,
	   'NAME' => $name,
	   'VALUE' => $value
	));
}
$arResult = array();
$user = new CUser;
if($_GET['key'] === '123'):
	$userData = json_decode(file_get_contents("php://input"), true);
	$checkUser = $user::GetList(($by = 'ID'), ($order = 'ASC'), array('EMAIL' => $userData['email']));
	if($checkUser->SelectedRowsCount() != 0):
		while($oldUser = $checkUser->Fetch()):
			if(intval($user->Update($oldUser['ID'], array('NAME' => $userData['name'], 'LAST_NAME' => $userData['name'], 'XML_ID' => $userData['xml_id'], 'ACTVE' => $userData['active'] == true ? 'Y' : 'N', 'UF_QUANTITY' => $userData['quantity'] == true ? 1 : 0))) > 0):
				$arResult['status'] = 'OK';
				$arResult['message'] = $oldUser['ID'];
				$getProfiles = CSaleOrderUserProps::GetList(array(), array('USER_ID' => $oldUser['ID']));
				while($profile = $getProfiles->Fetch()):
					CSaleOrderUserProps::Delete($profile['ID']);
				endwhile;
				if(!empty($userData['profiles'])):
					foreach($userData['profiles'] as $key => $array):
						if($array['pfTypePaymant'] == 1):
							$profileID = CSaleOrderUserProps::Add(array(
								'NAME' => $array['pfName'],
								'USER_ID' => $oldUser['ID'],
								'PERSON_TYPE_ID' => 1
							));
							if($array['pfLocation'] == ''):
								if($array['pfCity'] != ''):
									$locations = CSaleLocation::GetList(array(), array('CITY_NAME' => $array['pfCity']), false, false, array());
									if($getLocation = $locations->Fetch()):
										$location = $getLocation['CITY_ID'];
									endif;
								else:
									$location = $array['pfLocation'];
								endif;
							else:
								$location = $array['pfLocation'];
							endif;
							UserPropsValue($profileID, 1, 'Ф.И.О.', $array['pfFIO']);
							UserPropsValue($profileID, 2, 'E-Mail', $array['pfEMAIL']);
							UserPropsValue($profileID, 3, 'Телефон', $array['pfPhone']);
							UserPropsValue($profileID, 4, 'Индекс', $array['pfZip']);
							UserPropsValue($profileID, 5, 'Город', $array['pfCity']);
							UserPropsValue($profileID, 6, 'Местоположение', $location);
							UserPropsValue($profileID, 7, 'Адрес доставки', $array['pfAddresDelivery']);
						elseif($array['pfTypePaymant'] == 2):
							$profileID = CSaleOrderUserProps::Add(array(
								'NAME' => $array['pfName'],
								'USER_ID' => $oldUser['ID'],
								'PERSON_TYPE_ID' => 2
							));
							if($array['pfLocation'] == ''):
								if($array['pfCity'] != ''):
									$locations = CSaleLocation::GetList(array(), array('CITY_NAME' => $array['pfCity']), false, false, array());
									if($getLocation = $locations->Fetch()):
										$location = $getLocation['CITY_ID'];
									endif;
								else:
									$location = $array['pfLocation'];
								endif;
							else:
								$location = $array['pfLocation'];
							endif;
							UserPropsValue($profileID, 8, 'Название компании', $array['pfNameOrg']);
							UserPropsValue($profileID, 9, 'Юридический адрес', $array['pfAddresLegal']);
							UserPropsValue($profileID, 10, 'ИНН', $array['pfINN']);
							UserPropsValue($profileID, 11, 'КПП', $array['pfKPP']);
							UserPropsValue($profileID, 12, 'Контактное лицо', $array['pfFIO']);
							UserPropsValue($profileID, 13, 'E-Mail', $array['pfEMAIL']);
							UserPropsValue($profileID, 14, 'Телефон', $array['pfPhone']);
							UserPropsValue($profileID, 16, 'Индекс', $array['pfZip']);
							UserPropsValue($profileID, 17, 'Город', $array['pfCity']);
							UserPropsValue($profileID, 18, 'Местоположение', $location);
							UserPropsValue($profileID, 19, 'Адрес доставки', $array['pfAddresDelivery']);
							UserPropsValue($profileID, 20, 'Подтвердите, что являетесь ЮЛ или ИП', 'Y');
						endif;
					endforeach;
				endif;
			else:
				$arResult['status'] = 'ERROR';
				$arResult['message'] = $user->LAST_ERROR;
			endif;
		endwhile;
	else:
		do
		{
			$securityPolicy = \CUser::GetGroupPolicy([2]);
			$password = generate_password();
			$errors = (new \CUser)->CheckPasswordAgainstPolicy($password, $securityPolicy);
			if($errors == null):
				$err = 0;
			else:
				$err = 1;
			endif;
		}while($err > 0);
		$newUser = $user->Add(
			Array(
				'NAME' => $userData['name'],
				'LAST_NAME' => $userData['name'],
				'LOGIN' => $userData['login'],
				'EMAIL' => $userData['login'],
				'XML_ID' => $userData['xml_id'],
				'ACTVE' => $userData['active'] == true ? 'Y' : 'N',
				'UF_QUANTITY' => $userData['quantity'] == true ? 1 : 0,
				'LID' => 's1',
				'PASSWORD' => $password,
				'CONFIRM_PASSWORD' => $password
			)
		);
		if(intval($newUser) > 0):
			CUser::SendUserInfo($newUser, 's1', 'Вы успешно зарегистрированы на сайте!', false, 'USER_INFO');
			$arResult['status'] = 'OK';
			$arResult['message'] = $newUser;
			if(!empty($userData['profiles'])):
				foreach($userData['profiles'] as $key => $array):
					if($array['pfTypePaymant'] == 1):
						$profileID = CSaleOrderUserProps::Add(array(
							'NAME' => $array['pfName'],
							'USER_ID' => $newUser,
							'PERSON_TYPE_ID' => 1
						));
						UserPropsValue($profileID, 1, 'Ф.И.О.', $array['pfFIO']);
						UserPropsValue($profileID, 2, 'E-Mail', $array['pfEMAIL']);
						UserPropsValue($profileID, 3, 'Телефон', $array['pfPhone']);
						UserPropsValue($profileID, 4, 'Индекс', $array['pfZip']);
						UserPropsValue($profileID, 5, 'Город', $array['pfCity']);
						UserPropsValue($profileID, 6, 'Местоположение', $array['pfLocation']);
						UserPropsValue($profileID, 7, 'Адрес доставки', $array['pfAddresDelivery']);
					elseif($array['pfTypePaymant'] == 2):
						$profileID = CSaleOrderUserProps::Add(array(
							'NAME' => $array['pfName'],
							'USER_ID' => $newUser,
							'PERSON_TYPE_ID' => 2
						));
						UserPropsValue($profileID, 8, 'Название компании', $array['pfNameOrg']);
						UserPropsValue($profileID, 9, 'Юридический адрес', $array['pfAddresLegal']);
						UserPropsValue($profileID, 10, 'ИНН', $array['pfINN']);
						UserPropsValue($profileID, 11, 'КПП', $array['pfKPP']);
						UserPropsValue($profileID, 12, 'Контактное лицо', $array['pfFIO']);
						UserPropsValue($profileID, 13, 'E-Mail', $array['pfEMAIL']);
						UserPropsValue($profileID, 14, 'Телефон', $array['pfPhone']);
						UserPropsValue($profileID, 16, 'Индекс', $array['pfZip']);
						UserPropsValue($profileID, 17, 'Город', $array['pfCity']);
						UserPropsValue($profileID, 18, 'Местоположение', $array['pfLocation']);
						UserPropsValue($profileID, 19, 'Адрес доставки', $array['pfAddresDelivery']);
						UserPropsValue($profileID, 20, 'Подтвердите, что являетесь ЮЛ или ИП', 'Y');
					endif;
				endforeach;
			endif;
		else:
			$arResult['status'] = 'ERROR';
			$arResult['message'] = $user->LAST_ERROR;
		endif;
	endif;
	$arResult = array('result' => $arResult);
else:
	$arResult['status'] = 'ERROR';
	$arResult['message'] = 'Неверный ключ.';
endif;
$this->includeComponentTemplate();
?>