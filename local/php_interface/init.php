<?
use Bitrix\Main\Loader;
require_once($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/it_currency_lang.php');

AddEventHandler("main", "OnProlog", "redirectToRightSite");
function redirectToRightSite() {
    $baseDomain = "trodos.net";
    if ($_COOKIE["redirectToDomin"] != "Y" && strpos($_SERVER["REDIRECT_URL"], "/bitrix/") === false) {
        $ipAddress = Bitrix\Main\Service\GeoIp\Manager::getRealIp();
        $cityFullInfo = Bitrix\Main\Service\GeoIp\Manager::getDataResult($ipAddress, "ru");
        $cityes = ["Москва" => "msk." . $baseDomain, "Санкт-Петербург" => "spb." . $baseDomain];
        $regions = ["default" => $baseDomain, "Московская область" => "msk." . $baseDomain, "Ленинградская область" => "spb." . $baseDomain];
        if ($cityFullInfo AND $cityFullInfo->isSuccess()) {
            $regionName = $cityFullInfo->getGeoData()->regionName;
            $cityName = $cityFullInfo->getGeoData()->cityName;
            if ($regionName || $cityName) {
                $currSite = $_SERVER["HTTP_HOST"];
                if ($regions[$regionName]) {
                    $needSite = $regions[$regionName];
                } elseif ($cityes[$cityName]) {
                    $needSite = $cityes[$cityName];
                } else {
                    $needSite = $regions["default"];
                }
                if ($currSite != $needSite) {
                    setcookie("redirectToDomin", "Y", time()+7200);
                    LocalRedirect("https://" . $needSite . "/");
                }
            }
        }
    }
}

AddEventHandler("main", "OnEpilog", "MetaTags");
function MetaTags()
{
    global $APPLICATION;
    $keywords = $APPLICATION->GetProperty("keywords"); ;
    $title = $APPLICATION->GetProperty("title");
    $description = $APPLICATION->GetProperty("description");
    $replaced = ["#REGION_NAME_PP#", "#REGION_NAME_RP#", "#REGION_NAME#"];
    switch (SITE_ID) {
        case "s1":
            $keywords = str_replace($replaced, ["Новосибирске", "Новосибирска", "Новосибирск"], $keywords);
            $title = str_replace($replaced, ["Новосибирске", "Новосибирска", "Новосибирск"], $title);
            $description = str_replace($replaced, ["Новосибирске", "Новосибирска", "Новосибирск"], $description);
            break;
        case "s2":
            $keywords = str_replace($replaced, ["Санкт-Петербурге", "Санкт-Петербурга", "Санкт-Петербург"], $keywords);
            $title = str_replace($replaced, ["Санкт-Петербурге", "Санкт-Петербурга", "Санкт-Петербург"], $title);
            $description = str_replace($replaced, ["Санкт-Петербурге", "Санкт-Петербурга", "Санкт-Петербург"], $description);
            break;
        case "s3" :
            $keywords = str_replace($replaced, ["Москве", "Москвы", "Москва"], $keywords);
            $title = str_replace($replaced, ["Москве", "Москвы", "Москва"], $title);
            $description = str_replace($replaced, ["Москве", "Москвы", "Москва"], $description);
            break;
        default:
            break;
    }
    $APPLICATION->SetPageProperty("title", $title);
    $APPLICATION->SetPageProperty("keywords", $keywords);
    $APPLICATION->SetPageProperty("description", $description);
}

Loader::registerAutoLoadClasses(
	'currency',
	array(
		'CCurrencyLang' => '/local/php_interface/it_currency_lang.php',
	)
);

\CJSCore::RegisterExt(
	'currency',
	array(
		'js' => '/local/php_interface/it_core_currency.js',
		'rel' => array('core', 'main.polyfill.promise')
	)
);
\CJSCore::Init(array("currency"));


//**************************************************************************
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "SetVid");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "SetVid");
function SetVid(&$arFields){
	
	if($arFields['IBLOCK_ID']==15){//Только торговые предложения
		$arFields['PROPERTY_VALUES']['128']['VALUE'] = 7972;//Вид=Товар
	}
}

AddEventHandler("iblock", "OnBeforeIBlockPropertyAdd", "itllekt_CheckPropHIT");
AddEventHandler("iblock", "OnBeforeIBlockPropertyUpdate", "itllekt_CheckPropHIT");

function itllekt_CheckPropHIT(&$arFields){

	if($arFields['ID']==47){//HIT
		$arFields['NAME']='Наши предложения';
	}
//$results = print_r($arFields, true);
//$fd_data	= fopen($_SERVER["DOCUMENT_ROOT"]."/log_HIT.txt", "a");
//fwrite($fd_data, $results."\r\n");
//fclose($fd_mail);
	
}


/*
//**************************************************************************
AddEventHandler("sale", "OnBeforeOrderAdd", "itllekt_OnBeforeOrderAdd");
AddEventHandler("sale", "OnBeforeOrderUpdate", "itllekt_OnBeforeOrderUpdate");
function itllekt_OnBeforeOrderAdd(&$arFields) {
		
	//$orderFields['COMMENTS'] = $orderFields['COMMENTS'].'/n/r'.print_r($orderFields['ORDER_PROP'], true);
	 
}

function itllekt_OnBeforeOrderUpdate($ID, &$arFields) {
		
	$results = print_r($arFields['ORDER_PROP'], true);
//	$arFields['COMMENTS'] = $arFields['COMMENTS']."\r\n".$results;
 	$arFields['COMMENTS'] = $arFields['COMMENTS']."\r\n".'laksjdlaksjdalksdjjaksd';


$fd_data	= fopen($_SERVER["DOCUMENT_ROOT"]."/log3.txt", "a");
fwrite($fd_data, $arFields['COMMENTS']."\r\n");
fclose($fd_mail);
}
*/

//+Айтиллект[Макейкин Д.В.][08.07.2021] Перехват отправки сообщения
//Перехват события отправки почтового сообщения при покупке в 1 клик и добавление поля "Город"
AddEventHandler("main", "OnBeforeEventAdd", "CityToEmail");
function CityToEmail(&$event, &$lid, &$arFields)
{
	if($event==='NEW_ONE_CLICK_BUY'):
		foreach($arFields as $keyField => $arField):
			if($keyField==='RS_ORDER_ID'):
				$arOrder = \Bitrix\Sale\PropertyValueCollection::getList([
					'select' => ['*'],
					'filter' => ['=ORDER_ID' => $arField]
				]);
				while($arProperties = $arOrder->fetch()):
					foreach($arProperties as $key => $value):
						if($key==='NAME' && $value==='Город'):
							$arFields['CITY'] = $arProperties['VALUE'];
						endif;
					endforeach;
				endwhile;
			endif;
		endforeach;
	endif;
}
//Функция записи логов
function my_log($myArFields)
{
	$log_file_name = $_SERVER['DOCUMENT_ROOT']."/my_log_new_order.txt";
	$now = date("Y-m-d H:i:s");
	$results = print_r($myArFields, true);
	$fd_data = fopen($log_file_name, "a");
	fwrite($fd_data, $now."\r\n	".$results."\r\n");
	fclose($fd_data);
}

//Переменная для включения/отключения функции приобретения товаров только для авторизованных пользователей (false - отключено, любое другое значение - включено)
define('CHECK_AUTHORIZE', false);

//Перехват события обновления корзины и проверка авторизации пользователя
use Bitrix\Main;
Main\EventManager::getInstance()->addEventHandler('sale', 'OnSaleBasketItemRefreshData', 'CheckAuthorize');
function CheckAuthorize(Bitrix\Main\Event $event)
{
	global $USER;
    if (!$USER->IsAuthorized() && !CHECK_AUTHORIZE === false):
		return new \Bitrix\Main\EventResult(\Bitrix\Main\EventResult::ERROR, new \Bitrix\Sale\ResultError('Для приобритения товаров необходима авторизация', 'code'), 'sale');
	endif;
};
//-Айтиллект

//+Макейкин Д.В.[13.10.2021] Доработка дубля функции из ядра (Файл COptimus.php) для вывода остатка числом или строкой
function myGetQuantityArray($totalCount, $arItemIDs = array(), $useStoreClick="N"){
	global $APPLICATION, $USER;
	if(!is_object($USER)){
		$USER = new CUser();
	};
	$default = COption::GetOptionString("aspro.optimus", "USE_WORD_EXPRESSION");
	if($USER->IsAuthorized()):
		$getProp = CUser::GetList(($by="id"), ($order="asc"), array("ID" => CUser::GetID()), array("SELECT" => array("UF_QUANTITY"), "FIELDS" => array("ACTIVE")));
		if($prop = $getProp->Fetch()):
			$prop["UF_QUANTITY"] == 1 ? $check = "N" : $check = "Y";
		endif;
	endif;
	static $arQuantityOptions, $arQuantityRights;
	if($arQuantityOptions === NULL){
		$arQuantityOptions = array(
			"USE_WORD_EXPRESSION" => isset($check) ? $check : COption::GetOptionString("aspro.optimus", "USE_WORD_EXPRESSION", "Y", SITE_ID),
			"MAX_AMOUNT" => COption::GetOptionString("aspro.optimus", "MAX_AMOUNT", "10", SITE_ID),
			"MIN_AMOUNT" => COption::GetOptionString("aspro.optimus", "MIN_AMOUNT", "2", SITE_ID),
			"EXPRESSION_FOR_MIN" => COption::GetOptionString("aspro.optimus", "EXPRESSION_FOR_MIN", GetMessage("EXPRESSION_FOR_MIN_DEFAULT"), SITE_ID),
			"EXPRESSION_FOR_MID" => COption::GetOptionString("aspro.optimus", "EXPRESSION_FOR_MID", GetMessage("EXPRESSION_FOR_MID_DEFAULT"), SITE_ID),
			"EXPRESSION_FOR_MAX" => COption::GetOptionString("aspro.optimus", "EXPRESSION_FOR_MAX", GetMessage("EXPRESSION_FOR_MAX_DEFAULT"), SITE_ID),
			"EXPRESSION_FOR_EXISTS" => COption::GetOptionString("aspro.optimus", "EXPRESSION_FOR_EXISTS", GetMessage("EXPRESSION_FOR_EXISTS_DEFAULT"), SITE_ID),
			"EXPRESSION_FOR_NOTEXISTS" => COption::GetOptionString("aspro.optimus", "EXPRESSION_FOR_NOTEXISTS", GetMessage("EXPRESSION_FOR_NOTEXISTS_DEFAULT"), SITE_ID),
			"SHOW_QUANTITY_FOR_GROUPS" => (($tmp = COption::GetOptionString("aspro.optimus", "SHOW_QUANTITY_FOR_GROUPS", "", SITE_ID)) ? explode(",", $tmp) : array()),
			"SHOW_QUANTITY_COUNT_FOR_GROUPS" => (($tmp = COption::GetOptionString("aspro.optimus", "SHOW_QUANTITY_COUNT_FOR_GROUPS", "", SITE_ID)) ? explode(",", $tmp) : array()),
		);
		$arQuantityRights = array(
			"SHOW_QUANTITY" => false,
			"SHOW_QUANTITY_COUNT" => false,
		);

		global $USER;
		$res = CUser::GetUserGroupList($USER->GetID());
		while ($arGroup = $res->Fetch()){
			if(in_array($arGroup["GROUP_ID"], $arQuantityOptions["SHOW_QUANTITY_FOR_GROUPS"])){
				$arQuantityRights["SHOW_QUANTITY"] = true;
			}
			if(in_array($arGroup["GROUP_ID"], $arQuantityOptions["SHOW_QUANTITY_COUNT_FOR_GROUPS"])){
				$arQuantityRights["SHOW_QUANTITY_COUNT"] = true;
			}
		}
	}

	$indicators = 0;
	$totalAmount = $totalText = $totalHTML = $totalHTMLs = '';

	if($arQuantityRights["SHOW_QUANTITY"]){
		if($totalCount > $arQuantityOptions["MAX_AMOUNT"]){
			$indicators = 3;
			$totalAmount = $arQuantityOptions["EXPRESSION_FOR_MAX"];
		}
		elseif($totalCount < $arQuantityOptions["MIN_AMOUNT"] && $totalCount > 0){
			$indicators = 1;
			$totalAmount = $arQuantityOptions["EXPRESSION_FOR_MIN"];
		}
		else{
			$indicators = 2;
			$totalAmount = $arQuantityOptions["EXPRESSION_FOR_MID"];
		}

		if($totalCount > 0){
			if($arQuantityRights["SHOW_QUANTITY_COUNT"]){
				$totalHTML = '<span class="first'.($indicators >= 1 ? ' r' : '').'"></span><span class="'.($indicators >= 2 ? ' r' : '').'"></span><span class="last'.($indicators >= 3 ? ' r' : '').'"></span>';
			}
			else{
				$totalHTML = '<span class="first r"></span>';
			}
		}
		else{
			$totalHTML = '<span class="null"></span>';
		}

		//$totalText = ($totalCount > 0 ? $arQuantityOptions["EXPRESSION_FOR_EXISTS"] : $arQuantityOptions["EXPRESSION_FOR_NOTEXISTS"]);
		if($totalCount > 0){
			$totalText = $arQuantityOptions["EXPRESSION_FOR_EXISTS"];
		}else{
			if($useStoreClick=="Y"){
				$totalText = "<span class='store_view'>".$arQuantityOptions["EXPRESSION_FOR_NOTEXISTS"]."</span>";
			}else{
				$totalText = $arQuantityOptions["EXPRESSION_FOR_NOTEXISTS"];
			}
		}

		if($arQuantityRights["SHOW_QUANTITY_COUNT"] && $totalCount > 0){
			if($arQuantityOptions["USE_WORD_EXPRESSION"] == "Y"){
				if(strlen($totalAmount)){
					if($useStoreClick=="Y"){
						$totalText = "<span class='store_view'>".$totalAmount."</span>";
					}else{
						$totalText = $totalAmount;
					}
				}
			}
			else{
				if($useStoreClick=="Y"){
					$totalText .= (strlen($totalText) ? " <span class='store_view'>(".$totalCount.")</span>" : "<span class='store_view'>".$totalCount."</span>");
				}else{
					$totalText .= (strlen($totalText) ? " (".$totalCount.")" : $totalCount);
				}
			}
		}
		$totalHTMLs ='<div class="item-stock" '.($arItemIDs["STORE_QUANTITY"] ? "id=".$arItemIDs["STORE_QUANTITY"] : "").'>';
		$totalHTMLs .= '<span class="icon '.$arClass[1].($totalCount > 0 ? 'stock' : ' order').'"></span><span class="value">'.$totalText.'</span>';
		$totalHTMLs .='</div>';
	}

	$arOptions = array("OPTIONS" => $arQuantityOptions, "RIGHTS" => $arQuantityRights, "TEXT" => $totalText, "HTML" => $totalHTMLs);

	foreach(GetModuleEvents(OPTIMUS_MODULE_ID, 'OnAsproGetTotalQuantityBlock', true) as $arEvent) // event for manipulation store quantity block
		ExecuteModuleEventEx($arEvent, array($totalCount, &$arOptions));

	return $arOptions;
}
//-Макейкин Д.В.