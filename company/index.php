<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?><!--img width="226" alt="Логотип Антик-226.jpg" src="/upload/medialibrary/94b/94b71ec219e1f77884dee9ccf303688c.jpg" height="226" align="left" class="about-img"--> <!--img alt="antic-logo.png" src="/upload/aspro.optimus/4a7/4a70bb97fb46341e2f4becf8c01cc871.png" title="antic-logo.png" width="150" height="auto" class="about-img" align="left"--> <img width="150" src="/local/templates/trodos.net/images/antic-logo.svg" class="about-img" align="left">
<p style="text-align: justify;">
 <b>Компания "Антик"</b> - производитель и оптовый поставщик в Россию замочно-скобяных изделий, мебельной фурнитуры, аксессуаров для ванной комнаты и кухни.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 В 2020 году мы отметили свой 20-летний Юбилей под знаком постоянного развития: от небольшого предприятия до динамичной, стабильной Компании федерального уровня с собственными складскими помещениями в Новосибирске и Москве.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 На сегодняшний день мы продолжаем активную деятельность в оптовой торговле, поставляя напрямую товары собственных брендов Delphinium, TRODOS, Assol.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Мы гарантируем качество поставляемой продукции и несем ответственность за каждую проведенную сделку.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Профессионализм наших менеджеров обеспечивает высокий уровень сервиса и оперативную реакцию на изменения рынка. Наши сотрудники регулярно занимаются обновлением ассортимента, предлагая вашему вниманию трендовые актуальные новинки.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Ответственный подход к бизнесу, успешное решение всех возникающих задач, собственное производство товаров высокого качества - основные принципы работы <b>Компании "Антик"</b>.&nbsp;&nbsp;
</p>
<p style="text-align: justify;">
 <a href="/upload/bz/презентация%20компании%2028.04%20согл.pdf" target="_blank">Ссылка на презентацию Компании "Антик"</a><br>
</p>
<p>
 <br>
	 &nbsp;
</p>
<p>
</p>
<ol>
</ol><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>