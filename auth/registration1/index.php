<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
	$APPLICATION->SetTitle("Регистрация");

	if(!$USER->IsAuthorized())
	{?>

Уважаемые клиенты!<br>
В связи с разработкой нового личного кабинета пользователя на сайте Компании "Антик" просим:<br>
* Если вы уже являетесь клиентом нашей Компании и зарегистрированы в личном кабинете - подтвердить регистрацию у менеджера, после чего авторизоваться.<br>
<br>
<!--noindex--><a  class="button short" href="/auth/" rel="nofollow" >АВТОРИЗОВАТЬСЯ</a><!--/noindex-->&nbsp;<br><br>
* Если пока вы еще не являетесь нашим клиентом, пожалуйста, заполните анкету регистрации нового клиента, после чего вы сможете авторизоваться и начать работу в личном кабинете.<br>
 <br>
<!--noindex--><a  class="button short" href="https://b24-m4n7wi.bitrix24.site/crm_form3/" rel="nofollow" target="_blank">РЕГИСТРАЦИЯ</a><!--/noindex-->&nbsp;<br>

<?
	} elseif(!empty( $_REQUEST["backurl"] )) {LocalRedirect( $_REQUEST["backurl"] );} else { LocalRedirect(SITE_DIR.'personal/');}

	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>